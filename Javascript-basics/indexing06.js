// String indexing 

let firstName = "raman";

//  r    a   m   a   n    
//  0    1   2   3   4 

// console.log(firstName[0]);
// length of string 
// firstName.length 

console.log(firstName.length);

console.log(firstName[firstName.length-2]);

// last Index : length - 1 as num start from zero 