// here we declare constants 
// constants declared cannot be changed it will be fixed 
// also constants cannot be declared after initializing variable
// eg : const pi;
// pi = 3.14
// this is not allowded

const pi = 3.14;
console.log(pi);

