// object destructuring

const band = {
    bandName: "zipline",
    famousSong: "stair at mountains",
    year: 1968,
    anotherFamousSong: "northen",
  };
  
  let { bandName, famousSong, ...restProps } = band;
  console.log(bandName);
  console.log(restProps);