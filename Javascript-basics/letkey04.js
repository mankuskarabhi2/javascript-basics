// let keyword 
// here we declare variable with let keyword 

let firstName = "raman";
firstName = "suresh";
console.log(firstName);

// we should prefer using let keyword over var