"use strict";
// used to restrict user from not following strict js coding procedures

// variables can store any information

// declare a variable 

var firstName = "raman";

// use a variable to print  the data
console.log(firstName);

// change value of variable

firstName = "suresh";

// print the updated data again
console.log(firstName);