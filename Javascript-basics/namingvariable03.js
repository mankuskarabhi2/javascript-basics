// rules for naming variables 

// we cannot start with number  
// 1value (invalid)
// value1 (valid)

var great1 = 2;
console.log(great1);

// convention 
// start with small letter and use camelCase 