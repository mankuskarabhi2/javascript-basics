// here we will look at
// trim() 
// toUpperCase()
// toLowerCase()
// slice()

let firstName = " raman ";

 console.log(firstName.length);
// this will display the length of string raman

firstName = firstName.trim(); // "harshit"
console.log(firstName)
console.log(firstName.length);

firstName = firstName.toUpperCase();
//this will convert the string to uppercase "RAMAN"

// firstName = firstName.toLowerCase();

//this will convert the string to lowercase "raman"

// console.log(firstName);

let newString = firstName.slice(1); // rama
console.log(newString);