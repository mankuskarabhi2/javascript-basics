// objects inside array 
// this is very useful in real world applications

const users = [
    {userId: 1,firstName: 'ramesh', gender: 'male'},
    {userId: 2,firstName: 'suresh', gender: 'male'},
    {userId: 3,firstName: 'hitesh', gender: 'male'},
]
for(let user of users){
    console.log(user.firstName);
}